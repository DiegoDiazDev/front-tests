
## Configurar el proyecto
```
1) Instala todas las dependencias - npm install
2) Despliega el servidor de desarrollo - npm run serve
3) Compila el proyecto (opcional) - npm run build
```

## Rutas del proyecto
```
1) Prueba 1, giphy client: http://localhost:8080/
2) Prueba 2, sport api: http://localhost:8080/world-cups
```

## Carpetas donde se guarda los archivos de cada proyecto
```
1) Prueba 1, giphy client: src/modules/puppy-app
2) Prueba 2, sport api: src/modules/world-cups
```

## Detalles sobre la arquitectura y la estructura de carpetas
```
La arquitectura del proyecto está basada en vertical slice architecture,
esta consiste en separar cada funcionalidad en módulos aislados de tal
forma que cada módulo sea lo más independiente  posible.
De esta manera se pueden crear proyectos que puedan escalar con una excelente organización.

La arquitectura divide el proyecto en dos módulos principales
  
  1) src/application
    Aquí guardamos todo lo que será global en toda la aplicación o lo que se repite en más de una ocasión: 
    assets globales, estilos globales, componentes globales, etc.

  2) src/modules
    Aquí guardamos cada una de las singularidades de la aplicación comenzando 
    por cada una de los test y a su vez, dentro de cada test, se guardan cada una de las vistas y 
    cada uno de los componentes que son únicos para cada prueba
  
  Se puede consultar más información en el readme que hay en cada prueba
```


## Herramientas utilizadas en este proyecto y su uso
```
vue: Todo el proyecto trabaja con el framework vuejs

sass: Preprocesador de css que nos ayuda a mejorar la sintaxis de css y añadirle nuevas funcionalidades 

node-sass: Nos ayuda a crear estilos de sass globales para que no tengamos que importar en cada componente por separado los mixins globales, las variables globales, etc.

BEM: Los estilos scss fueron diseños con la metodología  BEM e implementa la sintaxys de scss creada para poder trabajar con esta metodología en sass

Prettier: Nos ayuda a mantener un código consistente en toda la aplicación con las mismas tabulaciones y saltos de línea en todo el proyecto

Eslint: Nos ayuda a implementar buenas prácticas en la sintaxys de nuestro código 

Gsap: La prueba puppy-app implementa gsap para crear animaciones de transición y animaciones lineales, consultar el readme de puppy-app para más detalles

Uso de alias: El proyecto implementa tres alias que hacen referencia a cada uno de los módulos, los alias nos sirven para poder escribir rutas más cortas y esto ayuda a volver el código más legible para todos los desarrolladores

  1) '@' - Hace referencia a la carpeta src
  2) '@app' - Hace referencia a la carpeta src/application
  3) '@mod' - Hace referencia a la carpeta src/modules

```