## Estructura de carpetas
```
Este es el módulo application, su alias es @app
en esta carpeta se deben guardar todo lo que es global en la aplicación o todos los elementos que se repiten en más de una vistas, componente, o funciones. Algunos de los ejemplos de archivos que podemos poner aquí son
  
  - Componentes globales
  - Assets globales
  - Estilos globales
  - Mixins de vue
  - Utilerias
  - El manejo del store (en caso de que se use vuex, pinia o algún otro)

Actualmente cuenta con tres carpetas
  assets - Aquí se encuentran los recursos que se usan en varias partes de la app
  theme - Aquí se encuentra todo el scss global
```