import 'babel-polyfill'
import Vue from 'vue'
import App from './App.vue'
import router from './app.router'
import '@app/theme/_global.scss'

Vue.config.productionTip = false

new Vue({
  router,
  render: (h) => h(App)
}).$mount('#app')
