import PuppyAppView from '@mod/puppy-app/PuppyAppView'
import { PuppyApp } from '@mod/puppy-app/PuppyApp.routes'

import WorldCupsView from './world-cups/WorldCupsView'
import { WorldCups } from '@mod/world-cups/WorldCups.routes'

export const routes = [
  {
    path: '/',
    component: PuppyAppView,
    children: PuppyApp
  },
  {
    path: '/world-cups',
    component: WorldCupsView,
    children: WorldCups
  }
]
export default routes
