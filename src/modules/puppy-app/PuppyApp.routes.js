import PuppySearcherView from './modules/puppy-searcher/PuppySearcherView'
import PuppyResultsView from './modules/puppy-results/PuppyResultsView'

export const PuppyApp = [
  {
    path: '/',
    name: 'puppy search',
    component: PuppySearcherView
  },
  {
    path: '/results/:query',
    name: 'puppy results',
    component: PuppyResultsView
  }
]
