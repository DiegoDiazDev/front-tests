## Estructura de carpetas
```
Este es el módulo de la applicación puppy-app.

En la carpeta components deben ir cada uno de los componentes que se repiten en más de una ocación dentro de esta aplicación

En la carpeta módules deben ir (encapsulados en sus propios módulos) cada una de las vistas a su vez cada uno de los componentes que requieren las vistas

En la carpeta services encontramos cada uno de los servicios que se necesitan en varias partes de la aplicación

Al asignar un lugar para cada uno de las funcionalidades que se repiten logramos una arquitectura orientada a la reutilización del código.

En el archivo PuppyApp.routes sé incluye cada una de las rutas de las vistas de la app, este archivo se importa desde el archivo principal de rutas, de esta manera las rutas de cada módulo también se aislan para trabajar por separado

El archivo PuppyApp es el layout principal de esta aplicación, aquí añadimos los componentes que se van a repetir en todas las vistas de la app
```