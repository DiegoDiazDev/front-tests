// Importamos la libreria gsap
import { gsap } from 'gsap'
import { CSSPlugin } from 'gsap/CSSPlugin'
gsap.registerPlugin(CSSPlugin)

export const closePageTransition = () => {
  const gsapTimelineInit = gsap.timeline()
  gsapTimelineInit.to('.animation-loader', {
    y: '100vh',
    zIndex: '-1',
    opacity: 0,
    duration: 2
  }, 'start')
}
