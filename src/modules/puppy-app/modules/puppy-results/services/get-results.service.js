const apiKey = process.env.VUE_APP_GIPHY_API
export const getResults = async (query) => {
  const apiUrl = `http://api.giphy.com/v1/gifs/search?api_key=${apiKey}&q=dogs ${query}&limit=9`
  const results = await fetch(apiUrl).then(response => response.json())
  return results.data
}
