// Importamos la libreria gsap
import { gsap } from 'gsap'
import { CSSPlugin } from 'gsap/CSSPlugin'
import router from '@/app.router'
gsap.registerPlugin(CSSPlugin)

export const entranceAnimations = () => {
  const gsapTimelineInit = gsap.timeline()
  gsapTimelineInit.to('.revelation-2', {
    x: '-100vh',
    duration: 2
  }, 'start')
  gsapTimelineInit.to('.revelation-1', {
    y: '-100%',
    duration: 2,
    delay: 0.5
  }, 'start')
  gsapTimelineInit.from('.animation-2', {
    y: '100%',
    opacity: 0,
    duration: 2,
    delay: 1
  }, 'start')
  gsapTimelineInit.from('.animation-3', {
    y: '100%',
    opacity: 0,
    duration: 2,
    delay: 2
  }, 'start')
}

export const exitAnimations = (query) => {
  const gsapTimelineInit = gsap.timeline()
  gsapTimelineInit.to('.animation-exit-1', {
    duration: 1,
    opacity: 0
  }, 'start')
  gsapTimelineInit.to('.animation-exit-2', {
    duration: 1,
    opacity: 0
  }, 'start')
  gsapTimelineInit.to('.animation-3', {
    opacity: 0,
    duration: 1
  }, 'start')
  gsapTimelineInit.to('.animation-2', {
    opacity: 0,
    duration: 1
  }, 'start')
  gsapTimelineInit.to('.animation-loader', {
    top: '0%',
    duration: 2,
    opacity: 1,
    delay: -1,
    background: 'white'
  })
  gsapTimelineInit.eventCallback('onComplete', function () {
    setTimeout(() => {
      router.push(`/results/${query}`)
    }, 1000)
  })
}
