export const saveQuery = (query) => {
  const historyOfQuery = JSON.parse(localStorage.getItem('historySaved') || '[]')
  historyOfQuery.push({
    query
  })
  localStorage.setItem('historySaved', JSON.stringify(historyOfQuery))
}
