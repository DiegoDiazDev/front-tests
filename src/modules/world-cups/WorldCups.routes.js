import PlaysCountryView from './modules/plays-country/PlaysCountryView'
import PlaysOfCountryView from './modules/plays-country/PlaysOfCountryView'
import SeasonsWomanCupsView from './modules/seasons-woman-cups/SeasonsWomanCupsView.vue'
import SeasonsWorldCupsView from './modules/seasons-world-cups/SeasonsWorldCupsView'

export const WorldCups = [
  {
    path: '/',
    name: 'Plays by country',
    component: PlaysCountryView
  },
  {
    path: '/plays-of-country/:id/:country',
    name: 'Plays of country',
    component: PlaysOfCountryView
  },
  {
    path: '/seasons-woman-cups',
    name: 'Seasons woman cups',
    component: SeasonsWomanCupsView
  },
  {
    path: '/seasons-world-cups',
    name: 'Seasons world cups',
    component: SeasonsWorldCupsView
  }
]
