// En caso de que se agoten las solicitudes permitidas crear una cuenta nueva y reemplazar el apiKey
const apiKey = process.env.VUE_APP_SPORTS_API
/**
* @returns Array de las ligas del mundo
*/
export const getLeagesOfWorld = async () => {
  const leagesOfWorldUrl = `https://app.sportdataapi.com/api/v1/soccer/countries?apikey=${apiKey}`
  const results = await fetch(leagesOfWorldUrl).then(response => response.json()).then((response) => {
    return response.data
  })
  return results
}

/**
* @param { number } idOfCountry - Id del pais del que s eobtendrán los equipos
* @returns Array con los equipos del pais
*/
export const getTeamsOfCountry = async (idOfCountry) => {
  const teamsByCountryUrl = `https://app.sportdataapi.com/api/v1/soccer/teams?apikey=${apiKey}&country_id=${idOfCountry}`
  const results = await fetch(teamsByCountryUrl).then(response => response.json()).then((response) => {
    return response.data
  })
  return results
}
