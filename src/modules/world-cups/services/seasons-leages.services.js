const apiKey = process.env.VUE_APP_SPORTS_API

/**
* getSeasons
* @description - Obtiene las temporadas por el id de la liga
* @param { number } leageId - Id de la liga
* @returns - Array con las temporadas de la liga
**/
export const getSeasons = async (leageId) => {
  const getSeasonsUrl = `https://app.sportdataapi.com/api/v1/soccer/seasons?apikey=${apiKey}&league_id=${leageId}`
  const results = await fetch(getSeasonsUrl).then(response => response.json()).then((response) => {
    return response.data
  })
  return results
}

/**
* getMatches
* @description - Obtiene los resultados de los partidos
* @param { number } seasonId - Id de la temporada de la que se obtendrán los partidos
* @param { date } dateFrom - Fechas de la temporada de la que se obtendrán los poartidos
* @returns - Array con los partidos de la temporada
*/
export const getMatches = async (seasonId, dateFrom) => {
  const getMatchesUrl = `https://app.sportdataapi.com/api/v1/soccer/matches?apikey=${apiKey}&season_id=${seasonId}&date_from=${dateFrom}`
  const results = await fetch(getMatchesUrl).then(response => response.json()).then((response) => {
    return response.data
  })
  return results
}
