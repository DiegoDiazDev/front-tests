const path = require('path')

const joinSrc = (s) => path.resolve(__dirname, 'src', s)

module.exports = {
  css: {
    loaderOptions: {
      scss: {
         additionalData: `
          @import "@/application/theme/main.scss";
        `
      }
    }
  },
  configureWebpack: {
    devtool: process.env.NODE_ENV === 'development' ? 'source-map' : 'inline-source-map',
    resolve: {
      extensions: [
        '.js',
        '.ts',
        '.json',
        '.vue'
      ],
      alias: {
        '@': path.resolve(__dirname, 'src'),
        '@app': joinSrc('application'),
        '@mod': joinSrc('modules')
      }
    }
  }
}
